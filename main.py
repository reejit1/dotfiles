import os

path="~/dotfiles/"

#Check if input is yes.
def remove_duplicate_lines(file_path):
    lines_seen = set()  # Track lines already seen
    output_lines = []  # Store unique lines

    # Read the file and process each line
    with open(file_path, 'r') as file:
        for line in file:
            line = line.strip()  # Remove leading/trailing whitespace

            # If the line is not a duplicate, add it to the output
            if line not in lines_seen:
                lines_seen.add(line)
                output_lines.append(line)

    # Write the unique lines back to the file
    with open(file_path, 'w') as file:
        file.write('\n'.join(output_lines))

def yes(input):
  yes=["y","Y","yes","Yes"]
  for i in yes:
    if i in input:
      return True
  else:
    return False



# F contains the content read


def bash():
    location=input("Name the location of the file.")
    folder=input("Give the destination of the file i.e. the folder name in this directory.")
    folder="./"+folder
    #check if folder exists if not make one
    if os.path.isdir(folder):
        pass
    else:
       os.makedirs(folder)
    
    #Copy the file in the folder
    print(location+" "+folder+"/")
    os.system("sudo cp -i -r "+location+" "+folder+"/") 

    with open("./main.sh", "r") as r:
        f=r.read().splitlines()
        f=f[-1]
        r.close()   
    with open("./main.sh", "a") as w:
        string="\n"+f"# To copy {location} to ./{location} to this directory"+"\n"+f"sudo cp -i -r {location} {folder}/"
        if string == r:
            pass
        else:
          w.write(string)
          w.close()
    consent=input("Add more files?")
    if yes(consent):
        bash()
    else:
        for root, dirs, files in os.walk("./"):
        # Iterate over subdirectories
         for folder in dirs:
            folder_path = os.path.join(root, folder)
            os.system("sudo chmod 777 *")

        # Iterate over files in current "./"
         for file in files:
            file_path = os.path.join(root, file)
            os.system("sudo chmod 777 * ")

         if root == "./":
            for file in files:
                file_path = os.path.join(root, file)
                os.system("sudo chmod 777 *")
        remove_duplicate_lines("./main.sh")
        os._exit(0)



def main():
    while True:
        consent=input("Do you want to add any files?")
        if yes(consent):
            bash()
        else:
           print("Bye.\nHave a nice day!")
           os._exit(0)

main()
