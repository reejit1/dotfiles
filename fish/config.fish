# Put system-wide fish configuration entries here
# or in .fish files in conf.d/
# Files in conf.d can be overridden by the user
# by files with the same name in $XDG_CONFIG_HOME/fish/conf.d

# This file is run by all fish instances.
# To include configuration only for login shells, use
# if status is-login
#    ...
# end
# To include configuration only for interactive shells, use
# if status is-interactive
#   ...
# end

set fish_greeting
colorscript random
alias backup='sudo timeshift --rsync --create'
alias update='sudo pacman -Syyu --noconfirm && yay --noconfirm'
alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'
alias ..='cd ..'
alias ...='cd ../..'
alias chistory='rm ~/.local/share/fish/fish_history'
starship init fish | source
alias cp='cp -i'
alias orphan='sudo pacman -Rns (pacman -Qdtq)'
alias ips="ip addr show | grep -oP '(?<=inet\s)\d+\.\d+\.\d+\.\d+'"
alias startssh="sudo systemctl start sshd&&echo 'Done'"
alias neofetch="neofetch --cpu_temp"
alias battery='cat /sys/class/power_supply/BAT0/capacity'
alias mirror='rate-mirrors arch | sudo tee /etc/pacman.d/mirrorlist'
alias update-full='sudo reflector --verbose --latest 10 --protocol https --protocol http --sort rate --save /etc/pacman.d/mirrorlist&&sudo pacman -Sy archlinux-keyring&&sudo pacman -Su&&yay -Syu --aur' 
alias journal='journalctl -b -p 3 -xb >> /home/reejit/error.txt&&nano /home/reejit/error.txt'
function fish_command_not_found
  bash ~/opt/command-not-found
end


function play_bell
    echo -ne "\a"
end

# Listen for the fish_mode_prompt event
#function fish_user_key_bindings
#    bind \e\[3\~ 'if string match --quiet "" (commandline); play_bell; end' --sync
#end
