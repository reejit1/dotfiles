
# To copy /home/reejit/.config/starship.toml to /home/reejit/.config/starship.toml to this directory.
sudo cp -i -r /home/reejit/.config/starship.toml ./starship/
# To copy /etc/sudoers to .//etc/sudoers to this directory.
sudo cp -i -r /etc/sudoers ./
# To copy /etc/fish/config.fish to .//etc/fish/config.fish to this directory.
sudo cp -i -r /etc/fish/config.fish ./fish/
# To copy ~/.config/fish/config.fish to ./~/.config/fish/config.fish to this directory.
#sudo cp -i -r /etc/spotify-adblock/config.toml ./spotify-adblock/
# To copy /etc/fstab to .//etc/fstab to this directory.
sudo cp -i -r /etc/fstab ./fstab/
# To copy /etc/hosts to .//etc/hosts to this directory.
sudo cp -i -r /etc/hosts ./hosts/
# To copy /etc/nanorc to .//etc/nanorc to this directory.
sudo cp -i -r /etc/nanorc ./nano/
# To copy /etc/pacman.conf to .//etc/pacman.conf to this directory.
sudo cp -i -r /etc/pacman.conf ./pacman/
# To copy /opt/sddm.jpg to .//opt/sddm.jpg to this directory.
sudo cp -i -r /opt/sddm.jpg ./Images/
# To copy ~/.config/latte/"My Layout.layout.latte" to ./~/.config/latte/"My Layout.layout.latte" to this directory.
sudo cp -i -r ~/.config/latte/"My Layout.layout.latte" ./latte/
# To copy ~/.config/whatsapp-for-linux/settings.conf to ./~/.config/whatsapp-for-linux/settings.conf to this directory.
sudo cp -i -r ~/.config/whatsapp-for-linux/settings.conf ./whatsapp-For-Linux/
# To copy ~/.config/xsettingsd/xsettingsd.conf to ./~/.config/xsettingsd/xsettingsd.conf to this directory.
sudo cp -i -r ~/.config/xsettingsd/xsettingsd.conf ./xsettings/
# To copy ~/.config/arkrc to ./~/.config/arkrc to this directory.
sudo cp -i -r ~/.config/arkrc ./ark/
# To copy ~/.config/dolphinrc to ./~/.config/dolphinrc to this directory.
sudo cp -i -r ~/.config/dolphinrc ./dolphin/
# To copy ~/opt/command-not-found to ./~/opt/command-not-found to this directory.
sudo cp -i -r ~/opt/command-not-found ./Misc/
# To copy ~/opt/wallpaperflare.com_wallpaper.jpg to ./~/opt/wallpaperflare.com_wallpaper.jpg to this directory.
sudo cp -i -r ~/opt/wallpaperflare.com_wallpaper.jpg ./Images/
# To copy ~/.xinitrc to ./~/.xinitrc to this directory.
sudo cp -i -r ~/.xinitrc ./xorg/
# To copy /etc/fish/config.fish to .//etc/fish/config.fish to this directory
# To copy /etc/X11/xorg.conf.d/20-i -rntel.conf to .//etc/X11/xorg.conf.d/20-i -rntel.conf to this directory
sudo cp -i -r /etc/X11/xorg.conf.d/20-i -rntel.conf ./xorg/
# To copy /boot/grub/grub.cfg to .//boot/grub/grub.cfg to this directory
sudo cp -i -r /boot/grub/grub.cfg ./grub/
# To copy /etc/grub.d/backup/etc_grub_d/40_custom to .//etc/grub.d/backup/etc_grub_d/40_custom to this directory
sudo cp -i -r /etc/grub.d/backup/etc_grub_d/40_custom ./grub/
# To copy /etc/default/grub to .//etc/default/grub to this directory
sudo cp -i -r /etc/default/grub ./grub/
# To copy /opt/pfp.jpg to .//opt/pfp.jpg to this directory
sudo cp -i -r /opt/pfp.jpg ./Images/
# To copy /opt/dolphin to .//opt/dolphin to this directory
sudo cp -i -r /opt/dolphin ./Misc/
# To copy /usr/share/applications/org.kde.dolphin.desktop  to .//usr/share/applications/org.kde.dolphin.desktop  to this directory
sudo cp -i -r /usr/share/applications/org.kde.dolphin.desktop  ./Misc/
# To copy ~/.config/neofetch/config.conf to ./~/.config/neofetch/config.conf to this directory
sudo cp -i -r ~/.config/neofetch/config.conf ./neofetch/
# To copy /home/reejit/Desktop/Grand Theft Auto IV - The Complete Edition/start.sh to .//home/reejit/Desktop/Grand Theft Auto IV - The Complete Edition/start.sh to this directory
sudo cp -i -r "/home/reejit/Desktop/Grand Theft Auto IV - The Complete Edition/commandline.txt" ./Games/
# To copy /home/reejit/.config/PulseEffects to .//home/reejit/.config/PulseEffects to this directory
sudo cp -i -r /home/reejit/.config/easyeffects ./EasyEffects
# To copy /home/reejit/.mozilla/firefox/xttp3u36.HArdened0/user.js to .//home/reejit/.mozilla/firefox/xttp3u36.HArdened0/user.js to this directory
sudo cp -i -r /home/reejit/.mozilla/firefox/xttp3u36.HArdened0/user.js ./Firefox/